const express = require('express');
const route = express.Router();

// Controllers
const Controller = require('./controller');

// Routes
route.get('/compact', Controller.compact);
route.get('/x', Controller.exchange);
route.get('/toplist', Controller.toplist);

module.exports = route;
