const axios = require('axios');
const cryptocompare = require('./helpers').cryptocompare;
const coinmarketcap = require('./helpers').coinmarketcap;
const _ = require('lodash');
const cryptos = require('cryptocurrencies');

const apiResponse = {};

class CryptoController {
  constructor() {
    this.compact = this.compact.bind(this);
    this.exchange = this.exchange.bind(this);
    this.toplist = this.toplist.bind(this);
  }

  async compact(req, res, next) {
    let querySymbols = req.query.symbols;
    let queryAmount = req.query.amount || '1';

    try {
      if (typeof querySymbols !== 'undefined' && querySymbols.length >= 2) {
        let symbols = querySymbols
          .trim()
          .toUpperCase()
          .split(' ')
          .join('')
          .split('/');
        let amount = parseFloat(
          queryAmount
            .trim()
            .split(' ')
            .join(''),
        );

        const fsym = symbols[0];
        const tsyms = symbols[1];

        try {
          const { data } = await cryptocompare.get('/price', {
            params: { fsym, tsyms },
          });
          if (typeof data[tsyms] !== 'undefined') {
            apiResponse.messages = [
              {
                text: `Meg is van! ${amount} ${fsym} mai árfolyamon ${data[
                  tsyms
                ] * amount} ${tsyms}-t ér.`,
              },
            ];
          }
          res.json(apiResponse);
        } catch (err) {
          next(err); // Push error to the next catch
        }
      } else {
        apiResponse.messages = [
          {
            text:
              'Sajnos a kérdésedre nem tudok válaszolni. Valószínűleg valamelyik crypto szimbólumát vagy a mennyiséget hibásan adtad meg.',
          },
        ];
        res.send(apiResponse);
      }
    } catch (err) {
      apiResponse.messages = [{ text: err.message }];
      res.send(apiResponse); // API error message
    }
  }

  async exchange(req, res, next) {
    let fromSymbol = req.query.from;
    let toSymbols = req.query.to;
    let queryAmount = req.query.amount || '1';

    try {
      if (
        typeof fromSymbol !== 'undefined' &&
        typeof toSymbols !== 'undefined' &&
        fromSymbol.length >= 3 &&
        toSymbols.length >= 3
      ) {
        const fsym = fromSymbol
          .trim()
          .toUpperCase()
          .split(' ')
          .join('')
          .split('/')[0];

        const tsyms = toSymbols
          .trim()
          .toUpperCase()
          .split(' ')
          .join('')
          .split('/')[0];

        const amount = parseFloat(
          queryAmount
            .trim()
            .split(' ')
            .join(''),
        );

        try {
          const { data } = await cryptocompare.get('/price', {
            params: { fsym, tsyms },
          });

          if (typeof data[tsyms] !== 'undefined' && !isNaN(amount)) {
            apiResponse.messages = [
              {
                text: `Meg is van! ${amount} ${fsym} mai árfolyamon ${data[
                  tsyms
                ] * amount} ${tsyms}-t ér.`,
              },
            ];
          }

          res.json(apiResponse);
        } catch (err) {
          next(err); // Push error to the next catch
        }
      } else {
        apiResponse.messages = [
          {
            text:
              'Sajnos a kérdésedre nem tudok válaszolni. Valószínűleg valamelyik crypto szimbólumát vagy a mennyiséget hibásan adtad meg.',
          },
        ];
        res.send(apiResponse);
      }
    } catch (err) {
      apiResponse.messages = [{ text: err.message }];
      res.send(apiResponse); // API error message
    }
  }

  async toplist(req, res, next) {
    const limit = req.query.limit || 10;
    const convert = req.query.convert || 'EUR';

    try {
      if (
        typeof limit !== 'undefined' &&
        !isNaN(parseInt(limit)) &&
        typeof convert !== 'undefined'
      ) {
        try {
          console.log(require.resolve('cryptocurrencies'));
          const elements = [];
          const { data } = await coinmarketcap.get('/', {
            params: { limit, convert },
          });
          if (_.isArray(data)) {
            _.map(data, (altcoin, i) => {
              const price = altcoin[`price_${convert.toLowerCase()}`];
              elements.push({
                title: `${altcoin.percent_change_24h < 0 ? '📉' : '📈'} ${
                  altcoin.name
                } (${altcoin.symbol})`,
                image_url: '',
                subtitle: `${price} ${convert} || Változás (24h): ${
                  altcoin.percent_change_24h
                }%`,
                buttons: [
                  {
                    type: 'web_url',
                    url: `https://coinmarketcap.com/currencies/${altcoin.id}`,
                    title: `${altcoin.name}`,
                  },
                ],
              });
            });
          }

          apiResponse.messages = [
            {
              attachment: {
                type: 'template',
                payload: {
                  template_type: 'list',
                  top_element_style: 'compact',
                  elements,
                },
              },
            },
          ];
          res.json(apiResponse);
        } catch (err) {
          next(err); // Push error to the next catch
        }
      } else {
        apiResponse.messages = [
          {
            text:
              'Sajnos a kérdésedre nem tudok válaszolni. Valószínűleg valamelyik crypto szimbólumát vagy a mennyiséget hibásan adtad meg.',
          },
        ];
        res.send(apiResponse);
      }
    } catch (err) {
      apiResponse.messages = [{ text: err.message }];
      res.send(apiResponse); // API error message
    }
  }
}

module.exports = new CryptoController();
