const axios = require('axios');

module.exports.cryptocompare = axios.create({
  baseURL: 'https://min-api.cryptocompare.com/data/',
  timeout: 9000,
  headers: { 'X-Requested-With': 'XMLHttpRequest' },
});

module.exports.coinmarketcap = axios.create({
  baseURL: 'https://api.coinmarketcap.com/v1/ticker/',
  timeout: 9000,
  headers: { 'X-Requested-With': 'XMLHttpRequest' },
});
