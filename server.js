const express = require('express');
const routes = require('./routes');
const app = express();

const PORT = process.env.PORT || 5000;

// Load api routes
app.use('/api/v1', routes);

// Send 404 if the api route does not exist
app.use((req, res, next) => {
  res.sendStatus(404);
});

app.listen(PORT, () => {
  console.log(`Chatfuel Bot-Server listening on port ${PORT}...`);
});
